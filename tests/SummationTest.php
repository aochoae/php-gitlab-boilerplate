<?php
declare(strict_types=1);

namespace Application\Test;

use PHPUnit\Framework\TestCase;

class SummationTest extends TestCase
{
    public function testSum(): void
    {
        $result = \Application\Summation::sum(5);
        
        $this->assertEquals($result, 15);
    }
}
