# PHP Boilerplate

Boilerplate for new PHP library hosted in GitLab.

## Requirements

* PHP version 7.2 or greater
* PHPUnit version 8 or greater

## Composer

    composer create-project --no-install --remove-vcs luisalberto/php-gitlab-boilerplate DIRECTORY dev-main

## Resources

* [PHP Supported Versions](https://php.net/supported-versions.php)
* [PHPUnit](https://phpunit.de/)
